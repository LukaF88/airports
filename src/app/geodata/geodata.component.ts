import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { Observable } from 'rxjs';
import {GeoData } from '../model/Geodata';

@Component({
  selector: 'app-geodata',
  templateUrl: './geodata.component.html',
  styleUrls: ['./geodata.component.css']
})
export class GeodataComponent implements OnInit {

  @Input()
  data!: Observable<GeoData[]>;

  @Output() nationSelected = new EventEmitter<GeodataComponent>();

  output: GeoData[] = [];
  nodes: TreeNode[];

  constructor() {
    this.nodes = [];
  }

  onClick(ob: any) {
    if (ob.node.parent && ob.node.data)
      this.nationSelected.emit(ob.node.data);
  }

  ngOnInit(): void {
    this.data.subscribe((result) => {
      this.output = result;
      this.nodes = result.map((el) =>{

        let nations: TreeNode<any>[];
        nations = el.nations.map((nat) => {
          return {
            key:  nat.alpha3,
            label: nat.name,
            data: nat,
            type: 'nazione',
            airports: nat.numAirports
          }
        })

        return {
          key: el.code,
          label: el.name,
          airports: el.numAirports,
          children: nations
        }
      });
     })
  }
}

