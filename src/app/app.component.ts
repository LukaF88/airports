import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Observable, of, forkJoin, from} from 'rxjs';
import { map, mergeMap} from 'rxjs/operators';
import { Airport } from './model/airport';
import { GeoData, GeoElement } from './model/Geodata';
import { AirportService } from './model/airport.service';
import { GeodataComponent } from './geodata/geodata.component';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

/**
 * OPENPOINTS
 *  mongolia: su grafico "undefined"
 */


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  selection : BehaviorSubject<GeoElement>;
  airportsDataModel: BehaviorSubject<Airport[]>;

  private airport$: Observable<Airport[]>;
  geodata$: Observable<GeoData[]>;
  private nation$: Observable<GeoElement[]>;
  private continent$: Observable<GeoData[]>;

  displayGeodata: boolean;
  private natsInfo: any;


  constructor(private service: AirportService) {
    this.airport$ = this.service.getAirports();
    this.continent$ = this.service.getGeoInfo();
    this.nation$ = this.service.getNations();
    this.geodata$ = this.createObs();
    this.natsInfo = {};
    this.displayGeodata = false;

    const initialStats: GeoElement = {
      alpha3: 'XXX',
      alpha2: 'XX',
      name: "INITIAL",
      numAirports: 0
    }

    const fakeAirport: Airport = {
      ident: '',
      type: '',
      name: '',
      elevation_ft: 0,
      continent: '',
      iso_country: '',
      iso_region: '',
      municipality: '',
      gps_code: '',
      iata_code: '',
      local_code: '',
      coordinates: ''
    }

    this.selection = new BehaviorSubject<GeoElement>(initialStats);
    this.airportsDataModel = new BehaviorSubject<Airport[]>([fakeAirport]);
  }

  getNotification($event: any){
    this.selection.next($event);
  }

  getNotificationNationCode($event: any){
    let val: any[];
    val = Object.values(this.natsInfo).filter((x: any) => x['IATA'] == $event);
    let nation : GeoElement = {
      alpha2: '' + val[0]['code'],
      alpha3: '' + val[0]['IATA'],
      name: val[0]['name'],
      numAirports: 0
    }

    this.getNotification(nation);
  }

  openGeodata(){
    this.displayGeodata=true;
  }

  createObs(): Observable<GeoData[]> {
    return forkJoin([this.airport$, this.continent$, this.nation$]).pipe(
      map(([airports, continents, nations]) => {

        let auxStruct: any = {};

        nations.forEach((nation: GeoElement) => {
          this.natsInfo[nation.alpha2] = { 'name': nation.name, 'IATA' : nation.alpha3, 'code': nation.alpha2};
        });

        continents.forEach((continent: GeoData) => {
          let res: GeoData =  {
            name: continent.name,
            code: continent.code,
            nations: [],
            numAirports: 0
          };
          if (!auxStruct[continent.code]){
            auxStruct[continent.code] = res;
            auxStruct[continent.code]['nationsCodes'] = [];
          }
      });

      airports.forEach((apt: Airport) => {
        apt.local_code = this.natsInfo[apt.iso_country]['IATA'];
        if (auxStruct[apt.continent].nationsCodes.indexOf(apt.iso_country) == -1){ // new nation
          auxStruct[apt.continent].nationsCodes.push(apt.iso_country);
            auxStruct[apt.continent].nations.push({
              alpha2: apt.iso_country,
              alpha3: this.natsInfo[apt.iso_country]['IATA'],
              name: this.natsInfo[apt.iso_country]['name'],
              numAirports: 1
            });
        }

        else { // update num of airports in that nation
          auxStruct[apt.continent].nations.forEach((el: GeoElement) => {
            if (el.alpha2 === apt.iso_country){
              el.numAirports++;
            }
          });
        }
        auxStruct[apt.continent].numAirports++;
       });

       this.airportsDataModel.next(airports);
       this.airportsDataModel.complete();

        let result = Object.values(auxStruct).map((el: any) => {

          let res: GeoData = {
            code: el.code,
            name: el.name,
            nations: el.nations.sort( //
              (a: GeoElement, b: GeoElement) => {
                return (b.name < a.name) ? 1 : -1;
              }),
            numAirports: el.numAirports
          }
          return res;
        });

        let randContinentIdx = Math.floor(Math.random() * Object.keys(result).length);
        let randNationIdx = Math.floor(Math.random() * result[randContinentIdx].nations.length);
        this.selection.next(result[randContinentIdx].nations[randNationIdx]);

        return result;
      })
      ) as Observable<GeoData[]>;
      }

  ngOnInit(): void {

    }

}
