import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http'
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { Airport } from './airport';
import { GeoData, GeoElement } from './Geodata';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  readonly BASE_URL = 'https://apps.zendownloader.com/airports/api';

  readonly ENDPOINT_AIRPORTS = this.BASE_URL + '/airports';
  readonly ENDPOINT_GEODATA = this.BASE_URL + '/geo';
  readonly ENDPOINT_NATIONS = this.BASE_URL + '/nations';

  constructor(private http: HttpClient) { }

  getNations() : Observable<GeoElement[]> {
    return this.http.get<GeoElement[]>(this.ENDPOINT_NATIONS).pipe(
      catchError(this.handleError('getNations'))
    ) as Observable<GeoElement[]>
  }

  getAirports() : Observable<Airport[]> {
    return this.http.get<Airport[]>(this.ENDPOINT_AIRPORTS).pipe(
      catchError(this.handleError('getAirports'))
    ) as Observable<Airport[]>
  }

  getGeoInfo() : Observable<GeoData[]> {
    return this.http.get<GeoData[]>(this.ENDPOINT_GEODATA).pipe(
      catchError(this.handleError('getGeodata'))
    ) as Observable<GeoData[]>
  }

  private handleError<T>(operation = 'operation'){
    return (error: HttpErrorResponse) : Observable<T> => {
      console.error(error);
      const message = (error.error instanceof ErrorEvent) ?
        error.error.message :
        `server returned code ${error.status} with body "${error.error}"`;

        throw new Error (`${operation} failed: ${message}`);


    }
  }
}
