export interface GeoData {
		name: string,
		code: string,
		nations: GeoElement[],
    numAirports: number
		}

export interface GeoElement {
    alpha3: string,
    alpha2: string,
    name: string,
    numAirports: number
}
