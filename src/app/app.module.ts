import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AirportService } from './model/airport.service';
import { MapComponent } from './map/map.component';
import { DisplayComponent } from './display/display.component';
import { DetailComponent } from './detail/detail.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations"
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { GeodataComponent } from './geodata/geodata.component';
import {TableModule} from 'primeng/table';
import {TreeModule} from 'primeng/tree';
import {ChartModule} from 'primeng/chart';
import {SidebarModule} from 'primeng/sidebar';
import {CardModule} from 'primeng/card';
import {BadgeModule} from 'primeng/badge';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DisplayComponent,
    DetailComponent,
    GeodataComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CalendarModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TreeModule,
    TableModule,
    ChartModule,
    SidebarModule,
    CardModule,
    BadgeModule
  ],
  providers: [AirportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
