import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Airport } from '../model/airport';
import { GeoElement } from '../model/Geodata';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  @Input()
  data!: Observable<Airport[]>;
  @Input()
  nationHandler$!: Observable<GeoElement>;

  nationToStats: any = {}

  selectedNation!: GeoElement;
  donut: any;
  options: any;

  constructor() {
    this.initChart();
  }

  private initChart(){
    this.donut = {
      labels: [],
      datasets: [
        {
          backgroundColor: [],
          hoverColor: [],
          data: []
        }
      ]
    }
  }

private generateColor() : string{
  let color= '';
  let codes = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];

  for(let i=0; i<6; i++)
    color += codes[Math.floor(Math.random() * 16)];
  return '#' + color;
}

  ngOnInit(): void {
    this.data.subscribe((airports: Airport[]) => {

      if (airports[0].ident == '')
        return;

      airports.forEach((apt: Airport) => { // mappo aeroporti per nazione
        if (!this.nationToStats[apt.iso_country])
          this.nationToStats[apt.iso_country] = {};
        if (!this.nationToStats[apt.iso_country][apt.municipality])
          this.nationToStats[apt.iso_country][apt.municipality] = 0;
        this.nationToStats[apt.iso_country][apt.municipality]++;
      });
    });

    this.nationHandler$.subscribe((nation: GeoElement) => { // selected nation changed
      if (nation.alpha2 === 'XX')
        return;
      this.initChart();
      this.selectedNation = nation;

      let statsData = this.nationToStats[nation.alpha2];
      if (statsData){
        Object.keys(statsData).forEach((key) => {
          let color = this.generateColor();

          this.donut.datasets[0].data.push(+statsData[key]);
          this.donut.datasets[0].backgroundColor.push(color);
          this.donut.datasets[0].hoverColor.push(color);
          this.donut.labels.push(key);

          this.options = {
            legend : {
              display: false
            }
          }
        })
      }
    });


  }
}
