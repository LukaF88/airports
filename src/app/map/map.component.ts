import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Airport } from '../model/airport';

import Map from 'ol/Map';
import View from 'ol/View';
import {Control, defaults} from 'ol/control';
import GeoJSON from 'ol/format/GeoJSON';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import Point from 'ol/geom/Point';
import { Feature, Overlay } from 'ol';
import Vector from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Select} from 'ol/interaction';
import {altKeyOnly, click, pointerMove, singleClick,} from 'ol/events/condition';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  @Input()
  data!: Observable<Airport[]>;

  output: any;
  map: any;
  markers: Feature[] = [];
  selectedNationId: string;


  @Output() nationCodeSelected = new EventEmitter<string>();

  constructor() {
    this.selectedNationId = '';
  }


  ngOnInit(){

    var popupContainer = document.getElementById("popup")!;
    var content = document.getElementById('popup-content')!;
    var closer = document.getElementById('popup-closer')!;

    const _overlay = new Overlay({
      element: popupContainer,
      autoPan: true,
      autoPanAnimation: {
        duration: 250,
      },
    });


    function closeOverlay() {
      _overlay.setPosition(undefined);
      closer.blur();
    }
    closer.onclick = function () {
      closeOverlay();
      return false;
    };

    this.data.subscribe((result) => {

    if (result[0].ident == '')
          return;

      this.output = result;
      result.forEach((el: Airport) => {
        let tkns = el.coordinates.split(",");

        let feature = new Feature({
          geometry: new Point(olProj.fromLonLat([+tkns[0], +tkns[1]])),
          name: el.name,
          lon: tkns[0],
          lat: tkns[1],
          alpha3: el.local_code
        });

        this.markers.push(feature);

      });

      const tileLayer = new TileLayer({
        source: new OSM()
      });

      const borders = new Vector({
        source: new VectorSource({
          url: 'assets/countries.geojson',
          format: new GeoJSON(),
        })
      });

      const aptMarkers = new Vector({
        source: new VectorSource({
          features: this.markers
        })
      })

      const layers = [tileLayer, borders, aptMarkers]

      this.map = new Map({
        target: 'airports_map',
        controls: defaults({ attribution: false }),
        layers: layers,
        overlays: [_overlay],
        view: new View({
          center: olProj.fromLonLat([0, 25]),
          zoom: 3
        })
      });


      const nationCodeSelected =  this.nationCodeSelected;
      const ref = this;

      const selectNationPointerMove = new Select({
        condition: pointerMove,
        filter: function(feature: any) {
          return feature.getId() !== undefined;
        }
      });

      const selectNationClick = new Select({
        condition: singleClick,
        filter: function(feature: any) {
          return feature.getId() !== undefined;
        }
      });

      const selectAirportClick = new Select({
        condition: singleClick,
        filter: function(feature: any) {
          return feature.getId() === undefined;
        }
      });

      const selectAirportHover = new Select({
        condition: pointerMove,
        filter: function(feature: any) {
          return feature.getId() === undefined;
        }
      });


      this.map.addInteraction(selectNationPointerMove);
      this.map.addInteraction(selectNationClick);
      this.map.addInteraction(selectAirportClick);
      this.map.addInteraction(selectAirportHover);

      selectNationClick.on('select', function (features: any) {
                // invio messaggio
                if (features.selected.length > 0){
                  let code : string = '' + features.selected[0].getId();
                  if (ref.selectedNationId !== code){
                    closeOverlay();
                    ref.selectedNationId = code;
                    nationCodeSelected.emit(code);
                  }
                  //console.log("-> " + code);
                }
      });

      selectAirportClick.on('select', function (features: any) {
        if (features.selected.length > 0){
            let name = features.selected[0].values_.name;
            const nationCode = features.selected[0].values_.alpha3;
            content.innerHTML = '<p>' + name + '</p>';
            _overlay.setPosition(olProj.fromLonLat([+features.selected[0].values_.lon, +features.selected[0].values_.lat]));
            if (ref.selectedNationId !== nationCode){
              ref.selectedNationId = nationCode;
              nationCodeSelected.emit(nationCode);
            }
          }
          //console.log("-> " + code);
      });
    });

  }
}
