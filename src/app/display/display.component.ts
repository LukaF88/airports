import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Airport } from '../model/airport';
import { GeoElement } from '../model/Geodata';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {


  @Input()
  data!: Observable<Airport[]>;
  @Input()
  nationHandler$!: Observable<GeoElement>;

  selectedNation!: GeoElement;
  date = new Date();
  nationToApt: any = {};
  columns : any[] = [];

  constructor() { }

  ngOnInit(): void {

    this.columns = [{
      "field": "ident",
      "label": "Code"
    },
    {
      "field": "name",
      "label": "Name"
    },
    {
      "field": "municipality",
      "label": "City"
    },
    {
      "field": "iata_code",
      "label": "IATA"
    },
    {
      "field": "coordinates",
      "label": "Coordinates"
    },


  ]

    this.data.subscribe((airports) => {
      airports.forEach((apt: Airport) => { // mappo aeroporti per nazione
        if (!this.nationToApt[apt.iso_country])
          this.nationToApt[apt.iso_country] = [];
        this.nationToApt[apt.iso_country].push(apt);
      });
    });

    this.nationHandler$.subscribe((nation: GeoElement) => { // selected nation changed
      console.log("Selection changed: " + JSON.stringify(nation));
      this.selectedNation = nation;
    });
  }
}
